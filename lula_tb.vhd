library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

entity lula_tb is
end lula_tb;

architecture lula_tb of lula_tb is
	signal op1: std_logic_vector(11 downto 0);
	signal op2:  std_logic_vector(11 downto 0);
	signal outLula :  std_logic_vector(11 downto 0);
	signal op_Lula :  std_logic_vector(3 downto 0);
begin 
	lula: entity work.lula
	port map(op1 => op1, op2 => op2, outLula => outLula, op_Lula => op_Lula);

	op_Lula <= x"1", x"2" after 10 ns, x"3" after 20 ns, x"4" after 30 ns, x"5" after 40ns, x"6" after 50 ns, x"7" after 60 ns;
	op1 <= x"001" , x"002" after 10 ns, "000000000001" after 15 ns, x"003" after 20 ns, x"004" after 30 ns;
	op2 <= x"001", x"002" after 10 ns, "000000000001" after 15 ns, x"003" after 20 ns, x"004" after 30 ns ;
	

end lula_tb;