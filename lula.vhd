
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

--Entidade
entity lula is
	port(op1, op2: in std_logic_vector(11 downto 0);
outLula : out std_logic_vector(11 downto 0);
op_Lula : in std_logic_vector(3 downto 0));
end lula;

architecture lula of lula is -- Opera��es internas
signal s : std_logic_vector(11 downto 0);
begin
	outLula <= s;
	s <= 
	op1 and op2 when op_Lula = "0001" else
	op1 or op2 when op_Lula = "0010" else
	op1 xnor op2 when op_Lula = "0011" else
	to_StdLogicVector(to_bitvector(op1) sll CONV_INTEGER(op2(11 downto 0))) when  op_Lula = "0100" else
	to_StdLogicVector(to_bitvector(op1) srl CONV_INTEGER(op2(11 downto 0 ))) when op_Lula = "0101" else
	op1+op2 when op_Lula = "0110" else
	not op1 + 1 when op_Lula = "0111" else
	op1;

end lula;